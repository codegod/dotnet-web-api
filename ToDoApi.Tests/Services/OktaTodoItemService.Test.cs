using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Services;
using TodoApi.Models;
using Xunit;
using Okta.Sdk;
using Okta.Sdk.Configuration;
using Microsoft.Extensions.Configuration;

namespace ToDoApi.Tests
{
    public class OktaTodoItemServiceTest
    {
        private ITodoItemService _service;
        private IOktaClient _client;
        public OktaTodoItemServiceTest()
        {
            var configuration = new ConfigurationBuilder().AddEnvironmentVariables().Build();
            var token = configuration.GetSection("CLIENT_TOKEN").Value;
            var host = configuration.GetSection("CLIENT_HOST").Value;
            _client = new OktaClient(new OktaClientConfiguration()
            {
                OrgUrl = host,
                Token = token
            });
            _service = new OktaTodoItemService(_client);
        }

        private async Task<IUser> GetUser()
        {
            try
            {
                var vader = await _client.Users.GetUserAsync("darth.vader@imperial-senate.gov");
                return vader;
            }
            catch (OktaApiException)
            {
                var vader = await _client.Users.CreateUserAsync(new CreateUserWithPasswordOptions
                {
                    // User profile object
                    Profile = new UserProfile
                    {
                        FirstName = "Anakin",
                        LastName = "Skywalker",
                        Email = "darth.vader@imperial-senate.gov",
                        Login = "darth.vader@imperial-senate.gov",
                    },
                    Password = "D1sturB1ng!",
                    Activate = true,
                });
                return vader;
            }

        }

        [Fact]
        public async void AddItem_ThrowsErrorWithUnexistentId()
        {
            try
            {
                var item = await _service.AddItem("random", "random");
            }
            catch (OktaApiException e)
            {
                Assert.IsType<OktaApiException>(e);
            }

        }

        [Fact]
        public async void AddItem_AddsItemToExistentResource()
        {
            var vader = await GetUser();

            var item = await _service.AddItem(vader.Id, "random");

            await _service.DeleteItem(vader.Id, item.Id);

            Assert.IsType<TodoItem>(item);
        }

        [Fact]
        public async void DeleteItem_DeletesTheItemWithGivenId()
        {
            var vader = await GetUser();
            var todos = await _service.GetItems(vader.Id);
            var count = todos.Count();

            var item = await _service.AddItem(vader.Id, "random");

            todos = await _service.GetItems(vader.Id);

            await _service.DeleteItem(vader.Id, item.Id);

            Assert.Equal(count + 1, todos.Count());
        }

        [Fact]
        public async void GetItems_GetsItemsCorrectly()
        {
            var vader = await GetUser();
            var todos = await _service.GetItems(vader.Id);
            var count = todos.Count();

            var item = await _service.AddItem(vader.Id, "random");

            todos = await _service.GetItems(vader.Id);

            await _service.DeleteItem(vader.Id, item.Id);

            Assert.Equal(count + 1, todos.Count());
        }
    }
}
