## .net Core 2 to-do REST API back-end application

[![pipeline status](https://gitlab.com/codegod/dotnet-web-api/badges/master/pipeline.svg)](https://gitlab.com/codegod/dotnet-web-api/commits/master)
[![coverage report](https://gitlab.com/codegod/dotnet-web-api/badges/master/coverage.svg?job=test)](https://gitlab.com/codegod/dotnet-web-api/commits/master)

### Setup
 - `cp .env.example .env # Don't forget to setup env vars`
 - `docker-compose up -d`
