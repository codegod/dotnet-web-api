using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;
using TodoApi.Services;
using Okta.Sdk;

namespace TodoApi.Controllers
{
    [Route("api/users")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly ITodoItemService _todoItemService;
        private readonly IUserService _userService;

        public UsersController(ITodoItemService todoItemService, IUserService userService)
        {
            _todoItemService = todoItemService;
            _userService = userService;
        }

        /// <summary>
        /// Gets a list of TodoItems.
        /// </summary>
        /// <returns>A list of TodoItems</returns> 
        [HttpGet("{uid}/todos")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [Produces("application/json")]
        [Authorize]
        public async Task<IActionResult> Get(string uid)
        {
            if (string.IsNullOrEmpty(uid)) return BadRequest();

            var todos = await _todoItemService.GetItems(uid);
            var todosInReverseOrder = todos.Reverse();

            return Ok(todosInReverseOrder);
        }

        /// <summary>
        /// Creates a new User.
        /// </summary> 
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(401)]
        [Produces("application/json")]
        [Authorize]
        public async Task<ActionResult<IUser>> AddUser([FromBody]UserModel newUser)
        {
            var user = await _userService.AddItem(newUser);

            if (user == null) return BadRequest();

            return CreatedAtAction(nameof(GetUserById), new { uid = user.Id }, user);
        }

        /// <summary>
        /// Creates a new TodoItem for given uid.
        /// </summary> 
        [HttpPost("{uid}/todos")]
        [ProducesResponseType(201)]
        [ProducesResponseType(401)]
        [Produces("application/json")]
        [Authorize]
        public async Task<ActionResult<TodoItem>> AddTodo(string uid, [FromBody]TodoItem newTodo)
        {
            if (string.IsNullOrEmpty(newTodo?.Text)) return BadRequest();

            if (string.IsNullOrEmpty(uid)) return BadRequest();

            var item = await _todoItemService.AddItem(uid, newTodo.Text);

            return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
        }

        /// <summary>
        /// Gets a specific user by given uid.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>User</returns>      
        [HttpGet("{uid}", Name = "GetUser")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [Produces("application/json")]
        [Authorize]
        public async Task<ActionResult<IUser>> GetUserById(string uid)
        {
            if (string.IsNullOrEmpty(uid)) return BadRequest();

            var user = await _userService.GetItem(uid);

            if (user == null) return NotFound();

            return Ok(user);
        }

        /// <summary>
        /// Gets a specific TodoItem for given uid.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A TodoItem for given guid</returns>      
        [HttpGet("{uid}/todos/{todoid:guid}", Name = "GetUserTodo")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [Produces("application/json")]
        [Authorize]
        public async Task<ActionResult<TodoItem>> GetById(string uid, Guid todoid)
        {
            if (string.IsNullOrEmpty(uid)) return BadRequest();

            var todos = await _todoItemService.GetItems(uid);
            var todo = todos.FirstOrDefault(item => item.Id == todoid);

            if (todo == null) return NotFound();

            return Ok(todo);
        }

        /// <summary>
        /// Updates a specific TodoItem for given uid.
        /// </summary>
        /// <param name="id"></param> 
        [HttpPut("{uid}/todos/{todoid:guid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [Authorize]
        public async Task<IActionResult> UpdateTodo(string uid, Guid todoid, [FromBody]TodoItem updatedData)
        {
            if (string.IsNullOrEmpty(uid)) return BadRequest();

            var updated = await _todoItemService.UpdateItem(uid, todoid, updatedData);

            if (!updated) return NotFound();

            return Ok();
        }

        /// <summary>
        /// Deletes a specific TodoItem for given uid.
        /// </summary>
        /// <param name="id"></param> 
        [HttpDelete("{uid}/todos/{todoid:guid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [Authorize]
        public async Task<IActionResult> DeleteTodo(string uid, Guid todoid)
        {
            if (string.IsNullOrEmpty(uid)) return BadRequest();

            await _todoItemService.DeleteItem(uid, todoid);

            return Ok();
        }
    }
}
