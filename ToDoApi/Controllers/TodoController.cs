﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;
using TodoApi.Services;

namespace TodoApi.Controllers
{
    [Route("api/todos")]
    [ApiController]
    [Authorize]
    public class TodosController : ControllerBase
    {
        private readonly ITodoItemService _todoItemService;

        public TodosController(ITodoItemService todoItemService)
        {
            _todoItemService = todoItemService;
        }

        private string _userId
        {
            get
            {
                return User.Claims
                    .FirstOrDefault(x => x.Type == "preferred_username")
                    ?.Value.ToString();
            }
        }

        // GET api/todos
        /// <summary>
        /// Gets a list of TodoItems.
        /// </summary>
        /// <returns>A list of TodoItems</returns> 
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [Produces("application/json")]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            if (string.IsNullOrEmpty(_userId)) return BadRequest();

            var todos = await _todoItemService.GetItems(_userId);
            var todosInReverseOrder = todos.Reverse();

            return Ok(todosInReverseOrder);
        }

        // GET api/todos
        /// <summary>
        /// Creates a new TodoItem.
        /// </summary> 
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(401)]
        [Produces("application/json")]
        [Authorize]
        public async Task<ActionResult<TodoItem>> AddTodo([FromBody]TodoItem newTodo)
        {
            if (string.IsNullOrEmpty(newTodo?.Text)) return BadRequest();

            if (string.IsNullOrEmpty(_userId)) return BadRequest();

            var item = await _todoItemService.AddItem(_userId, newTodo.Text);

            return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
        }

        /// <summary>
        /// Gets a specific TodoItem.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A TodoItem for given guid</returns>      
        [HttpGet("{id:guid}", Name = "GetTodo")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [Produces("application/json")]
        [Authorize]
        public async Task<ActionResult<TodoItem>> GetById(Guid id)
        {
            if (string.IsNullOrEmpty(_userId)) return BadRequest();

            var todos = await _todoItemService.GetItems(_userId);
            var todo = todos.FirstOrDefault(item => item.Id == id);

            if (todo == null) return NotFound();

            return Ok(todo);
        }

        /// <summary>
        /// Updates a specific TodoItem.
        /// </summary>
        /// <param name="id"></param> 
        [HttpPut("{id:guid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [Authorize]
        public async Task<IActionResult> UpdateTodo(Guid id, [FromBody]TodoItem updatedData)
        {
            if (string.IsNullOrEmpty(_userId)) return BadRequest();

            var updated = await _todoItemService.UpdateItem(_userId, id, updatedData);

            if (!updated) return NotFound();

            return Ok();
        }
        
        /// <summary>
        /// Deletes a specific TodoItem.
        /// </summary>
        /// <param name="id"></param> 
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [Authorize]
        public async Task<IActionResult> DeleteTodo(Guid id)
        {
            if (string.IsNullOrEmpty(_userId)) return BadRequest();

            await _todoItemService.DeleteItem(_userId, id);

            return Ok();
        }
    }
}
