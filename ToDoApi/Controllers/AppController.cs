using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Okta.Sdk;
using TodoApi.Models;

namespace TodoApi.Controllers
{
    [Route("/")]
    [ApiController]
    public class AppController : ControllerBase
    {
        private readonly IOptions<AppSettings> _settings;
        private readonly IOktaClient _oktaClient;

        public AppController(IOptions<AppSettings> settings, IOktaClient oktaClient = null)
        {
            _settings = settings;
            _oktaClient = oktaClient;
        }

        [HttpGet]
        public ActionResult<Application> Get()
        {
            return _settings.Value.Application;
        }

        [HttpGet("/login", Name = "Authenticate")]
        public IActionResult Login()
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return Challenge(OpenIdConnectDefaults.AuthenticationScheme);
            }

            return Redirect("/api/todos");
        }

        [HttpGet("/accounts/me", Name = "Account")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [Produces("application/json")]
        [Authorize]
        public async Task<MeModel> Me()
        {
            var username = User.Claims
                .FirstOrDefault(x => x.Type == "preferred_username")
                ?.Value.ToString();

            var me = new MeModel
            {
                Username = username,
                SdkAvailable = _oktaClient != null
            };

            if (!me.SdkAvailable)
            {
                return me;
            }

            if (!string.IsNullOrEmpty(username))
            {
                var user = await _oktaClient.Users.GetUserAsync(username);

                dynamic userInfoWrapper = new ExpandoObject();
                userInfoWrapper.Profile = user.Profile;
                userInfoWrapper.PasswordChanged = user.PasswordChanged;
                userInfoWrapper.LastLogin = user.LastLogin;
                userInfoWrapper.Status = user.Status.ToString();
                me.UserInfo = userInfoWrapper;

                me.Groups = (await user.Groups.ToList()).Select(g => g.Profile.Name).ToArray();
            }

            return me;
        }
    }
}
