## .net Core 2 to-do REST API back-end application

### Main requirements

1. Basic route / will return app name, version and hello message
2. Route /login will offer a user authentication functionality, that should be performed via OAuth2 using e-mail and password as input parameters
3. Once user is authenticated, the route /user/{userid}/todo will return a list of user todo, and route /user/{userid}/todo/id will return a list o available todo ids
4. Once user is authenticated, the route /user/{userid}/todo/{todoID} will return a single todo
5. Once user is authenticated, the route /user/{userid}/todo/add will add a todo to list
6. Once user is authenticated, the route /user/{userid}/todo/edit/{todoID} will allow to update a todo
7. Once user is authenticated, the route /user/{userid}/todo/delete/{todoID} will allow to remove a todo
8. Once user is authenticated, the route /user/{userid}/todo/search will allow to return a search in todo list based on input parameter

### Mandatory requirements:
- [x] Documented code
- [x] Readme file and gitignore done correctly
- [x] SwaggerUI/R-Studio documented API
- [x] Implementation of security principles
- [x] Well structured code
- [x] Exposed git repo : )

### Optional bonuses:
- [x] Deployed web application
- [x] Development of CD/CI pipeline from repo to environment
- [x] Obeying to linting rules and standards of .NET development
- [ ] UML diagramming of implemented architectural solution
