using System;

namespace TodoApi.Models
{
    public class TodoItem
    {
        public Guid Id { get; set; }

        public string Text { get; set; }

        public bool isCompleted { get; set; }
    }
}
