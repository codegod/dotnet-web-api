using System.Collections.Generic;
using Okta.Sdk;

namespace TodoApi.Models
{
    public class MeModel
    {
        public string Username { get; set; }

        public bool SdkAvailable { get; set; }

        public dynamic UserInfo { get; set; }

        public IEnumerable<string> Groups { get; set; }
    }

    public class UserModel
    {
        public UserProfile Profile;
        public string Password;
    }
}
