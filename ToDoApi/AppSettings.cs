public class AppSettings
{
    public Application Application { get; set; }
}

public class Application
{
    public string Name { get; set; }
    public string Version { get; set; }
    public string Message { get; set; }
}
