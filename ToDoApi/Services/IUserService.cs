using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;
using Okta.Sdk;

namespace TodoApi.Services
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetItems();

        Task<IUser> GetItem(string userId);

        Task<IUser> AddItem(UserModel user);

        Task<Boolean> UpdateItem(string userId);

        Task DeleteItem(string userId);
    }
}
