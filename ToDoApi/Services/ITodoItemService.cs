using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;

namespace TodoApi.Services
{
    public interface ITodoItemService
    {
        Task<IEnumerable<TodoItem>> GetItems(string userId);

        Task<TodoItem> AddItem(string userId, string text);

        Task<Boolean> UpdateItem(string userId, Guid id, TodoItem updatedData);

        Task DeleteItem(string userId, Guid id);
    }
}
