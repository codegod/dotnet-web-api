using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Okta.Sdk;
using TodoApi.Models;

namespace TodoApi.Services
{
    public class OktaUserService : IUserService
    {
        private const string TodoProfileKey = "nickName";

        private readonly IOktaClient _oktaClient;

        public OktaUserService(IOktaClient oktaClient)
        {
            _oktaClient = oktaClient;
        }

        public async Task<IUser> AddItem(UserModel user)
        {
            try
            {
                var createdUser = await _oktaClient.Users.CreateUserAsync(new CreateUserWithPasswordOptions
                {
                    Profile = new UserProfile
                    {
                        FirstName = user.Profile.FirstName,
                        LastName = user.Profile.LastName,
                        Email = user.Profile.Email,
                        Login = user.Profile.Login,
                    },
                    Password = user.Password,
                    Activate = true,
                });

                return createdUser;
            }
            catch (OktaApiException)
            {
                return null;
            }
        }

        public async Task<IUser> GetItem(string userId)
        {
            return await _oktaClient.Users.GetUserAsync(userId);
        }

        public Task<IEnumerable<User>> GetItems()
        {
            throw new NotImplementedException();
        }

        public Task<Boolean> UpdateItem(string userId)
        {
            throw new NotImplementedException();
        }

        public Task DeleteItem(string userId)
        {
            throw new NotImplementedException();
        }
    }
}
